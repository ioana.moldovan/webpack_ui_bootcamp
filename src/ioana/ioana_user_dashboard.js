import "../style.css";
try {
    var ioanaDashboard = [{
        id: 0,
        profileTitle: "Ioana's Profile",
        profileContent: {
            profileImg: {
                profileImgTitle: "Add URL: ",
                profileImgSrc: "../assets/profile_ioana.jpg",
                profileImgAlt: "Parrot"
            },
            profileFirstNameTitle: "First Name: ",
            profileLastNameTitle: "Last Name: ",
            profileFirstName: "Ioana",
            profileLastName: "Moldovan",
            profileAgeTitle: "Age: ",
            profileAge: 29,
            profileGenderTitle: "Gender: ",
            profileGender: "F",
            hobbies: {
                hobbiesTitle: "Hobbies: ",
                hobbiesContent: ["Dancing", "Reading", "Documentaries", "Makeup", "Long Walks"]
            }
        }
    },
        {
            id: 1,
            profileTitle: "Alexandru's Profile",
            profileContent: {
                profileImg: {
                    profileImgTitle: "Add URL: ",
                    profileImgSrc: "https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
                    profileImgAlt: "Man"
                },
                profileFirstNameTitle: "First Name: ",
                profileLastNameTitle: "Last Name: ",
                profileFirstName: "Alexandru",
                profileLastName: "Veres",
                profileAgeTitle: "Age: ",
                profileAge: 19,
                profileGenderTitle: "Gender: ",
                profileGender: "M",
                hobbies: {
                    hobbiesTitle: "Hobbies: ",
                    hobbiesContent: ["Cycling", "Swimming", "Movies", "Guitar", "Running"]
                }
            }
        },
        {
            id: 2,
            profileTitle: "Maria's Profile",
            profileContent: {
                profileImg: {
                    profileImgTitle: "Add URL: ",
                    profileImgSrc: "https://images.unsplash.com/photo-1562346531-6fd51e998566?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
                    profileImgAlt: "Parrot"
                },
                profileFirstNameTitle: "First Name: ",
                profileLastNameTitle: "Last Name: ",
                profileFirstName: "Maria",
                profileLastName: "Pop",
                profileAgeTitle: "Age: ",
                profileAge: 23,
                profileGenderTitle: "Gender: ",
                profileGender: "F",
                hobbies: {
                    hobbiesTitle: "Hobbies: ",
                    hobbiesContent: ["Reading", "Swimming", "Hiking", "Singing", "Tennis"]
                }
            }
        }
    ]

    let myProfiles = {
        showProfiles: function () {
            let dashboardContainer = document.querySelector(".dashboard-container");
            dashboardContainer.innerHTML = '';
            ioanaDashboard.forEach(profile => {
                var singleProfile = document.createElement("div");
                singleProfile.className = "singleProfile";
                singleProfile.innerHTML = `
            <div class="user-container">
                 <div class="header">
                      <h2>
                        ${profile.profileTitle}
                      </h2>
                    </div>
                    
                    <div class="profile-wraper">
                      <div class="common-item">
                        <img 
                        src=${profile.profileContent.profileImg.profileImgSrc}
                        alt=${profile.profileContent.profileImg.profileImgAlt} class="imageIoana"/>
                        <br>
                        <label for="changeInput" class="changeBtnLabel"><strong>Add URL: </strong></label>
                        <input class="profileEdit changeInput" readonly name="profileImgSrc">
                        <button class="profileBtn changeBtn" onclick="changePhoto(${profile.id})">Change photo</button>
                        
                         <p>
                          <strong>${profile.profileContent.hobbies.hobbiesTitle}</strong>
                          <ul class="hobbyList">
                            ${profile.profileContent.hobbies.hobbiesContent.map((item, index) => {
                    return `<li> ${item} <button class="profileBtn removeButton"  onclick='removeHobby(${profile.id}, ${index})'>Remove</button></li>`;
                }).join("")}
                          </ul>
                        </p>
                        
                         <input class="newHobby" name="addHobby">
                         <button class="profileBtn addBtn" onclick="addNewHobby(${profile.id})">Add new hobby</button>
                      </div>
                
                      <div class="common-item">      
                        <label for="firstName"><strong>${profile.profileContent.profileFirstNameTitle}</strong></label>
                        <input class="profileEdit firstName" readonly name="profileFirstName" oninput="handleChange(name, value, ${profile.id})" value="${profile.profileContent.profileFirstName}">
                        <br>
                       
                        <label for="lastName"><strong>${profile.profileContent.profileLastNameTitle}</strong></label>
                        <input class="profileEdit lastName" readonly name="profileLastName"  oninput="handleChange(name, value, ${profile.id})" value="${profile.profileContent.profileLastName}">
                        <br>
                        
                        <label for="age"><strong>${profile.profileContent.profileAgeTitle}</strong></label>
                        <input class="profileEdit age" readonly name="profileAge"  oninput="handleChange(name, value, ${profile.id})" value="${profile.profileContent.profileAge}">
                        <br>
                        
                        <label for="gender"><strong>${profile.profileContent.profileGenderTitle}</strong></label>
                        <input class="profileEdit gender" readonly  name="profileGender"  oninput="handleChange(name, value, ${profile.id})" value="${profile.profileContent.profileGender}">
                        <br>
                       
                        <button class="editButton profileBtn" onclick="enableProfileEdit(${profile.id})">Edit Info</button>
                        <button class="saveButton profileBtn" onclick="saveChanges(${profile.id})">Save Info</button>
                       
                      </div>
                    </div>
            </div>
            <br>
    `;

                dashboardContainer.appendChild(singleProfile);

            })
        },

        createBackButton: function () {
            let backParagraph = document.createElement("p");
            backParagraph.className = "back";
            document.querySelector(".dashboard-container").appendChild(backParagraph);
            let backButton = document.createElement("button");
            backButton.className = "profileBtn";
            backButton.innerText = "Back to main page";
            backParagraph.appendChild(backButton);

            backParagraph.addEventListener("click", () => {
                window.location.href = "../index.html";
            })
        },

        createAddNewUser: function () {
            let addUserParagraph = document.createElement("p");
            addUserParagraph.className = "add-user-container";
            let addUserBtn = document.createElement("button");
            addUserBtn.className = "addUserBtn profileBtn";
            addUserBtn.innerText = "Add New User"
            addUserParagraph.appendChild(addUserBtn);
            document.querySelector(".dashboard-container").appendChild(addUserParagraph);
            addUserBtn.addEventListener("click", addNewUserInputs);
        }

    }

    window.enableProfileEdit = function (id) {
        let userContainer = document.querySelectorAll(".user-container")[id];
        let inputs = userContainer.querySelectorAll(".profileEdit");
        for (var i = 0; i < inputs.length; i++) {

            inputs[i].readOnly = false;
            inputs[i].style.border = "1px solid black";
        }
        userContainer.querySelector(".changeBtn").style.display = "block";
        userContainer.querySelector(".changeBtnLabel").style.display = "block";
    }

    window.handleChange = function (name, value, id) {
        ioanaDashboard[id].profileContent[name] = value;
    }

    function changePhoto(id) {
        let userContainer = document.querySelectorAll(".user-container")[id];
        let changeInput = userContainer.querySelector(".changeInput");

        if (changeInput.value !== '') {
            ioanaDashboard[id].profileContent.profileImg.profileImgSrc = changeInput.value;
        }

        myProfiles.showProfiles();
        myProfiles.createBackButton();
    }

    window.saveChanges = function (id) {
        let userContainer = document.querySelectorAll(".user-container")[id];
        let inputs = userContainer.querySelectorAll(".profileEdit");
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].readOnly = true;
            inputs[i].style.borderStyle = "none";
        }
        userContainer.querySelector(".changeBtn").style.display = "none";
        userContainer.querySelector(".changeBtnLabel").style.display = "none";
        changePhoto(id);
    }


    window.addNewHobby = function (id) {
        let userContainer = document.querySelectorAll(".user-container")[id];
        let hobbyInput = userContainer.querySelector(".newHobby");
        let hobbyValue = hobbyInput.value;
        if(hobbyValue !== "") {
            ioanaDashboard[id].profileContent.hobbies.hobbiesContent.push(hobbyValue);
            myProfiles.showProfiles();
        }
    }

    window.removeHobby = function (id, index) {
        ioanaDashboard[id].profileContent.hobbies.hobbiesContent.splice(index, 1)
        // event.target.closest("li").remove();
        myProfiles.showProfiles();
    }

    function addNewUserInputs() {
        document.querySelector(".addUserBtn").style.display = "none"
        let addUserForm = document.createElement("div");
        addUserForm.className = "add-form-container";
        document.querySelector(".add-user-container").appendChild(addUserForm);
        addUserForm.innerHTML = `
        <h3>New User Info</h3>
        <form class="add-form" action="#" onsubmit="getValues(this, event)">
            <div class="add-input">
                <label for="addUserFirstName">Enter user first name: </label>
                <input name="addUserFirstName" placeholder="Your first name"><br>
            </div>
            <div class="add-input">
                <label for="addUserLastName">Enter user last name: </label>
                <input name="addUserLastName" placeholder="Your last name"><br>
            </div>
            <div class="add-input">
                <label for="addUserAge">Enter user age: </label>
                <input name="addUserAge" placeholder="Your age"><br>
            </div>
            <div class="add-input">
                <label for="addUserGender">Enter user gender: </label>
                <input name="addUserGender" placeholder="Your gender"><br>
            </div>
            <div class="add-input">
                <label for="imageUrl">Enter image url: </label>
                <input name="imageUrl" placeholder="Copy your URL here"><br>
            </div>
            <div class="add-input">
                <label for="addUserHobbies">Enter hobbies: </label>
                <input name="addUserHobbies" placeholder="Your hobbies"><br>
            </div>
            
            <button class="profileBtn add-user-btn" type="submit"> Submit user </button>
        </form> 
        `
    }

    window.getValues = function (form, event) {
        event.preventDefault();
        if (form.addUserFirstName.value && form.addUserLastName.value && form.addUserAge.value && form.addUserGender.value && form.imageUrl.value && form.addUserHobbies.value) {
            var newUser = {
                id: ioanaDashboard.length,
                profileTitle: `${form.addUserFirstName.value}'s Profile`,
                profileContent: {
                    profileImg: {
                        profileImgTitle: "Add URL: ",
                        profileImgSrc: form.imageUrl.value,
                        profileImgAlt: "New User"
                    },
                    profileFirstNameTitle: "First Name: ",
                    profileLastNameTitle: "Last Name: ",
                    profileFirstName: form.addUserFirstName.value,
                    profileLastName: form.addUserLastName.value,
                    profileAgeTitle: "Age: ",
                    profileAge: form.addUserAge.value,
                    profileGenderTitle: "Gender: ",
                    profileGender: form.addUserGender.value,
                    hobbies: {
                        hobbiesTitle: "Hobbies: ",
                        hobbiesContent: form.addUserHobbies.value.split(",")
                    }
                }
            };

            ioanaDashboard.push(newUser);

            myProfiles.showProfiles();
            myProfiles.createAddNewUser();
            myProfiles.createBackButton();

        } else {
            let alertField = document.createElement("p");
            alertField.innerText = "You have to fill all the fields";
            alertField.style.border = "1px solid red";
            let formContainer = document.querySelector(".add-form");
            formContainer.insertBefore(alertField, formContainer.lastElementChild);
        }
    }

    console.log("no errors");
    myProfiles.showProfiles();
    myProfiles.createAddNewUser();
    myProfiles.createBackButton();

} catch (error) {
    console.log(error);
}
