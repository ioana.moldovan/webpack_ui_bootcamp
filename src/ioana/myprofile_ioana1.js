import "../style.css";

var profileInfo = {

  profileTitle: "My Profile",
  profileContent: {
    profileImg: {
      profileImgSrc: "../assets/profile_ioana.jpg",
      profileImgAlt: "Parrot"
    }, //add object alternative -> 
    profileFirstNameTitle: "First Name: ",
    profileLastNameTitle: "Last Name: ",
    profileFirstName: "Ioana",
    profileLastName: "Moldovan",
    profileAgeTitle: "Age: ",
    profileAge: 29,
    profileGenderTitle: "Gender: ",
    profileGender: "F",
    hobbies: {
      hobbiesTitle: "Hobbies: ",
      hobbiesContent: ["Dancing", "Reading", "Documentaries", "Makeup", "Long Walks"]
    }
  },
  backButton: {
    backButtonTitle: "Back to main page",
    backButtonSrc: "../index.html" //pls add src as well :D
  }
};

var myNewProfile = {
    generateProfile: function () {
        document.body.innerHTML = `
    <div class="user-container" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
      <div class="header">
        <h2>
          ${profileInfo.profileTitle}
        </h2>
      </div>
      
      <div class="profile-wraper">
        <div class="common-item">
          <img 
          src=${profileInfo.profileContent.profileImg.profileImgSrc}
          alt=${profileInfo.profileContent.profileImg.profileImgAlt} class="imageIoana"/>
          <br>
          <input class="profileEdit changeInput" readonly name="profileImgSrc">
          <button class="profileBtn changeBtn" onclick="changePhoto()">Change photo</button>
          
           <p>
            <strong>${profileInfo.profileContent.hobbies.hobbiesTitle}</strong>
            <ul class="hobbyList">
              ${profileInfo.profileContent.hobbies.hobbiesContent.map(item => {
            return `<li> ${item} <button class="profileBtn removeButton">Remove</button></li>`;
        }).join("")}
            </ul>
          </p>
          
           <input class="newHobby" name="addHobby">
           <button class="profileBtn" onclick="addNewHobby()">Add new hobby</button>
        </div>
  
        <div class="common-item">      
          <label for="firstName"><strong>${profileInfo.profileContent.profileFirstNameTitle}</strong></label>
          <input class="profileEdit firstName" readonly  name="profileFirstName" oninput="handleChange(name, value)" value="${profileInfo.profileContent.profileFirstName}">
          <br>
         
          <label for="lastName"><strong>${profileInfo.profileContent.profileLastNameTitle}</strong></label>
          <input class="profileEdit lastName" readonly  name="profileLastName"  oninput="handleChange(name, value)" value="${profileInfo.profileContent.profileLastName}">
          <br>
          
          <label for="age"><strong>${profileInfo.profileContent.profileAgeTitle}</strong></label>
          <input class="profileEdit age" readonly name="profileAge"  oninput="handleChange(name, value)" value="${profileInfo.profileContent.profileAge}">
          <br>
          
          <label for="gender"><strong>${profileInfo.profileContent.profileGenderTitle}</strong></label>
          <input class="profileEdit gender" readonly  name="profileGender"  oninput="handleChange(name, value)" value="${profileInfo.profileContent.profileGender}">
          <br>
         
          <button class="editButton profileBtn" onclick="enableEdit()">Edit Info</button>
          <button class="saveButton profileBtn" onclick="saveChanges()">Save Info</button>
         
        </div>
      </div>
     
      <p class="back">
        <button class="profileBtn">${profileInfo.backButton.backButtonTitle}</button>
      </p> 
      <div class="countdown-container">
          <strong>Time remaining: </strong>
          <p class="countDown">
          <span>Days: <span class = "days"></span></span>
          <span>Hours: <span class = "hours"></span></span>
          <span>Minutes: <span class = "minutes"></span></span>
          <span>Seconds: <span class = "seconds"></span></span>
          </p>
      </div>
    </div>
    `;
        document.querySelector(".back").addEventListener("click", () => {
            window.location.href = `${profileInfo.backButton.backButtonSrc}`;
        })
    }
}


window.handleChange = (name, value) => {
  profileInfo.profileContent[name] = value;
  console.log(profileInfo)
}

window.addNewHobby = () => {
    let hobbyList = document.querySelector(".hobbyList");
    let hobbyInput = document.querySelector(".newHobby");
    let hobbyValue = hobbyInput.value;

    if(hobbyInput.value !== "") {
        let newLi = document.createElement("li");
        newLi.innerHTML = `${hobbyValue} <button class='profileBtn removeButton'>Remove</button>`;
        hobbyList.appendChild(newLi);
        hobbyInput.value = '';
    }
}


const removeHobby = (event) => {
  if (event.target.classList.contains("removeButton")) {
    event.target.closest("li").remove();
  }
}

window.enableEdit = () => {
  let inputs = document.querySelectorAll(".profileEdit");
  for (var i = 0; i < inputs.length; i++) {
    console.log(inputs[i])
    inputs[i].readOnly = false;
    inputs[i].style.borderStyle = "solid";
  }
  document.querySelector(".changeBtn").style.display = "block"
}

window.saveChanges = () => {
  let inputs = document.querySelectorAll(".profileEdit");
  for (var i = 0; i < inputs.length; i++) {
    console.log(inputs[i])
    inputs[i].readOnly = true;
    inputs[i].style.borderStyle = "none";
  }
  document.querySelector(".changeBtn").style.display = "none"
  
}

window.changePhoto = () => {
  let changeInput = document.querySelector(".changeInput");
  profileInfo.profileContent.profileImg.profileImgSrc = changeInput.value;
  myNewProfile.generateProfile();
}

function remainingTime () {
    var countDownDate = new Date("Nov 30, 2020 23:59:59 GMT-04:00").getTime();
    var now = new Date().getTime();
    var distance = countDownDate - now;


    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    return {distance, days, hours, minutes, seconds};
}

function countDown() {
    var countDownWrapper = document.querySelector(".countDown");
    var daysWrapper = document.querySelector(".days");
    var hoursWrapper = document.querySelector(".hours");
    var minutesWrapper = document.querySelector(".minutes");
    var secondsWrapper = document.querySelector(".seconds");

    var remainingTotal = remainingTime();

    daysWrapper.innerHTML = remainingTotal.days + ",";
    hoursWrapper.innerHTML = remainingTotal.hours + ",";
    minutesWrapper.innerHTML = remainingTotal.minutes + ",";
    secondsWrapper.innerHTML = remainingTotal.seconds;

    var updateCountdown = function () {
        var newRemainingTotal = remainingTime();

        var newDays = Math.floor(newRemainingTotal.distance / (1000 * 60 * 60 * 24));
        var newHours = Math.floor((newRemainingTotal.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var newMinutes = Math.floor((newRemainingTotal.distance % (1000 * 60 * 60)) / (1000 * 60));
        var newSeconds = Math.floor((newRemainingTotal.distance % (1000 * 60)) / 1000);


        if(remainingTotal.days !== newDays){
            remainingTotal.days = newDays;
            daysWrapper.innerHTML = newDays + ",";
        }

        if(remainingTotal.hours !== newHours){
            remainingTotal.hours = newHours;
            hoursWrapper.innerHTML = newHours + ",";
        }

        if(remainingTotal.minutes !== newMinutes) {
            remainingTotal.minutes = newMinutes;
            minutesWrapper.innerHTML = newMinutes + ",";
        }

        if(remainingTotal.seconds !== newSeconds) {
            remainingTotal.seconds = newSeconds;
            secondsWrapper.innerHTML = newSeconds;
        }

        if (remainingTotal.distance < 0) {
            clearInterval(x);
            countDownWrapper.innerHTML = "Congratulation!!";
        }
    };

    updateCountdown();
    var x = setInterval(updateCountdown, 1000);
}


myNewProfile.generateProfile();
document.querySelector("ul").addEventListener("click", removeHobby);
countDown();
