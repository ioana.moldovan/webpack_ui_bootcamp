import "../style.css";
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
export var profilePage = {
    title: "About",
    userInfo: {
        image: {
            src: "",
            alt: "Profile Picture"
        },
        firstName: "Mariana",
        lastName: "Frentsos",
        age: 22,
        gender: "Female",
        hobbies: {
            // add label for hobbies title
            hobbiesTitle: "Hobbies",
            hobbiesItems: ["hiking", "kayaking", "movies", "audio books"],
            getHobbies: function (array) {
                return array.map((element, index) => `<li class="hobbiesItems" value=${index}>${element}<button class="removeBtn" value=${index}>x</button></li>`).join("");
            }

        }
    },

    backButton: {
        backButtonLabel: "Back",
        src: "../index.html"
    },
    // add object format with label and src :D
    addHobbieBtnLabel: "Add Hobby",

    edditHobbieBtn: {
        label: "Edit"
    }
};

export var aboutPage = {

    displayDetails: function (profilePage) {
        var myBody = document.querySelector("body");

        myBody.innerHTML = `
    <div class="mypage-container">
   <h2>${profilePage.title}</h2>
   <div class="countdown-container"> 
   <h4>Time until graduation</h4>
    <div>
    <span id="days"></span>
    <span id="hours"></span>
    <span id="min"></span>
    <span id="sec"></span>
    <span id="end"></span>
    </div>
   </div>
   <div class="imgWrapper">
   <img
     src=${profilePage.userInfo.image.src}
     alt=${profilePage.userInfo.image.alt}
     id="profilePicture" />
   </div>
   <div class="details">
    <label for="firstName">First Name: </label>
     <input class="mainDetails" name="firstName" type="text" id="firstName" value='${profilePage.userInfo.firstName}' readonly>
    <label for="lastName">Last Name: </label>
     <input class="mainDetails" name="lastName" type="text" id="lastName" value='${profilePage.userInfo.lastName}'' readonly>
    <label for="gender">Gender: </label>
     <input class="mainDetails" name="gender" type="text" id="gender" value='${profilePage.userInfo.gender}' readonly>
    <label for="age">Age: </label>
     <input class="mainDetails" name="age" type="number" id="age" value='${profilePage.userInfo.age}' readonly>
     <div class="btns">
    <button id="editBtn">${profilePage.edditHobbieBtn.label}</button>
    <button id="saveBtn">Save</button>
    </div>
   <label>${profilePage.userInfo.hobbies.hobbiesTitle}</label>
      <ul class="hobbiesContainer">${profilePage.userInfo.hobbies.getHobbies(profilePage.userInfo.hobbies.hobbiesItems)} </ul>
      <div class="addContainer">
     <button id="addHobbie">${profilePage.addHobbieBtnLabel}</button>
     <input id="newHobbie" name=value>
     </div>
   </div>
    <button id="goBack">${profilePage.backButton.backButtonLabel}</button>
   </div>`;

        // document.querySelector("#goBack").addEventListener("click", function () {
        //     window.location.href = `${profilePage.backButton.src}`;
        // });

        document.querySelector("#addHobbie").addEventListener("click", addHobbie);
        document.querySelectorAll(".removeBtn").forEach(element => element.addEventListener("click", removeHobbie));

        document.querySelector("#editBtn").addEventListener("click", editDetails);
        document.querySelector("#saveBtn").addEventListener("click", saveDetails);
        countDown();

    },


};

let executed = false;

const changePicture = () => {
    let photoInput = document.querySelector(".photoInput");
    if (photoInput.value !== "") profilePage.userInfo.image.src = photoInput.value;
};

const saveDetails = () => {
    executed = false;
    let inputs = document.querySelectorAll(".mainDetails");
    changePicture();
    inputs.forEach(input => profilePage.userInfo[input.name] = input.value);
    updatePage("updated");
};


const createPhotoInput = () => {
    let sibling = document.querySelector("#age");
    let imageUrl = document.createElement("input");
    let editImgLabel = document.createElement("label");
    editImgLabel.innerHTML = "Photo Url: ";
    imageUrl.className = "photoInput";
    sibling.parentNode.insertBefore(imageUrl, sibling.nextSibling);
    imageUrl.parentNode.insertBefore(editImgLabel, sibling.nextSibling);
};

const editDetails = (() => {
    return () => {
        if (!executed) {
            executed = true;
            createPhotoInput();
        }

        let inputs = document.querySelectorAll(".mainDetails");
        document.querySelector("#saveBtn").style.visibility = "visible";
        inputs.forEach(input => {
            input.readOnly = false
            input.style.border = "0.5px solid black ";
            input.style.outline = "2px solid #cceeff";
        });
    };

})();


const addHobbie = () => {
    let newHobbie = document.querySelector("#newHobbie");
    if (newHobbie.value !== "") profilePage.userInfo.hobbies.hobbiesItems.push(newHobbie.value);
    updatePage("added");
};

const removeHobbie = () => {
    profilePage.userInfo.hobbies.hobbiesItems = profilePage.userInfo.hobbies.hobbiesItems.filter((hobbie, index) => index !== Number(event.target.value));
    updatePage("removed");
};

const updatePage = action => {
    profilePage.userInfo.hobbies.getHobbies(profilePage.userInfo.hobbies.hobbiesItems);
    aboutPage.displayDetails(profilePage);
};

const countDown = () => {

    let targetDate = new Date("November 21, 2020 23:59:59 GMT-04:00").getTime();
    let now = new Date().getTime();
    let timeLeft = targetDate - now;
    let days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
    let hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);
    document.querySelector("#sec").innerHTML = seconds + "s ";
    document.querySelector("#min").innerHTML = minutes + "m ";
    document.querySelector("#hours").innerHTML = hours + "h ";
    document.querySelector("#days").innerHTML = days + "d ";

    let myFunc = setInterval(function () {

        if (seconds <= 59) {
            seconds--;
            document.querySelector("#sec").innerHTML = seconds + "s ";
        }

        if (seconds < 0) {
            minutes--;
            document.querySelector("#min").innerHTML = minutes + "m ";
            seconds = 59;
            document.querySelector("#sec").innerHTML = seconds + "s ";
        }

        if (minutes < 0) {
            hours--;
            document.querySelector("#hours").innerHTML = hours + "h ";
            minutes = 59;
            document.querySelector("#min").innerHTML = minutes + "m ";

        }
        if (hours < 0) {
            days--;
            document.querySelector("#days").innerHTML = days + "d ";
            hours = 23;
            document.querySelector("#hours").innerHTML = hours + "h ";
        }

        if (timeLeft < 0) {
            clearInterval(myFunc);
            document.querySelector("#days").innerHTML = "";
            document.querySelector("#hours").innerHTML = "";
            document.querySelector("#min").innerHTML = "";
            document.querySelector("#sec").innerHTML = "";
            document.querySelector("#end").innerHTML = "Graduated!!";
        }

    }, 1000)
}
