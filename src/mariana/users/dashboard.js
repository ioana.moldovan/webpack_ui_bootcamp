import './dashboard.css';

try {
    var users = [{
        userInfo: {
            id: 0,
            image: {
                src: "../../../assets/marianapic.jpg",
                alt: "Profile Picture"
            },
            firstName: "Mariana",
            lastName: "Frentsos",
            age: 22,
            gender: "Female",
            hobbies: {
                // add label for hobbies title
                hobbiesTitle: "Hobbies",
                hobbiesItems: ["hiking", "kayaking", "movies", "audio books"],
                getHobbies: function (array) {
                    return array.map((element, index) => `<li class="hobbiesItems" value=${index}>${element}<button class="removeBtn" value=${index}>x</button></li>`).join("");
                }
            },

            pastImages: [
                "../../../assets/marianapic.jpg",
                'https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg',
                "https://images.squarespace-cdn.com/content/v1/5a5906400abd0406785519dd/1552662149940-G6MMFW3JC2J61UBPROJ5/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/baelen.jpg?format=1500w",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRnltfxyRHuEEUE4gIZp9fr77Q8goigP7mQ6Q&usqp=CAU"
            ]
        },
        getTitle: function () {
            return this.userInfo.firstName + " " + this.userInfo.lastName;
        },

        // add object format with label and src :D
        addHobbieBtnLabel: "Add Hobby",

        edditHobbieBtn: {
            label: "Edit"
        }
    },

        {
            userInfo: {
                id: 1,
                image: {
                    src: "https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340",
                    alt: "Profile Picture"
                },
                firstName: "Ana",
                lastName: "Doe",
                age: 32,
                gender: "Female",
                hobbies: {
                    // add label for hobbies title
                    hobbiesTitle: "Hobbies: ",
                    hobbiesItems: ["hiking", "kayaking", "movies", "audio books"],
                    getHobbies: function (array) {
                        return array.map((element, index) => `<li class="hobbiesItems" value=${index}>${element}<button class="removeBtn" value=${index}>x</button></li>`).join("");
                    }
                },

                pastImages: ["https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRnltfxyRHuEEUE4gIZp9fr77Q8goigP7mQ6Q&usqp=CAU",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRFU7U2h0umyF0P6E_yhTX45sGgPEQAbGaJ4g&usqp=CAU"
                ]
            },
            getTitle: function () {
                return this.userInfo.firstName + " " + this.userInfo.lastName;
            },

            addHobbieBtnLabel: "Add Hobby",

            edditHobbieBtn: {
                label: "Edit"
            }
        },
        {
            userInfo: {
                id: 2,
                image: {
                    src: "https://i.pinimg.com/originals/46/da/e5/46dae512e375bee2664a025507da8795.jpg",
                    alt: "Profile Picture"
                },
                firstName: "John",
                lastName: "Doe",
                age: 22,
                gender: "Male",
                hobbies: {
                    // add label for hobbies title
                    hobbiesTitle: "Hobbies",
                    hobbiesItems: ["hiking", "kayaking", "movies", "audio books"],
                    getHobbies: function (array) {
                        return array.map((element, index) => `<li class="hobbiesItems" value='${index}' >${element}<button class="removeBtn" value=${index}>x</button></li>`).join("");
                    }
                },
                pastImages: ["https://i.pinimg.com/originals/46/da/e5/46dae512e375bee2664a025507da8795.jpg",
                    "https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSOGvJp9EXLwTONXl4lLNolyhwPoeisNi7HWw&usqp=CAU"
                ]

            },
            getTitle: function () {
                return this.userInfo.firstName + " " + this.userInfo.lastName;
            },

            addHobbieBtnLabel:"Add Hobby",

            edditHobbieBtn: {
                label: "Edit"
            }
        }
    ];


    var usersDashboard = {

        displayDetails: function () {
            var usersContainer = document.querySelector(".users");
            usersContainer.innerHTML = "";
            users.forEach(user => {
                var userContainer = document.createElement("div");
                userContainer.className = "user-details";
                userContainer.id = user.userInfo.id;
                userContainer.innerHTML = `
          <h4 class="user-title">${user.getTitle()}</h4>
          <div class="imgWrapper">
          <img
            src=${user.userInfo.image.src}
            alt='${user.userInfo.image.alt}'
            class="profilePicture"/>
          </div>
         
          <div class="details">
            <label for="firstName">First Name: </label>
            <input class="mainDetails" name="firstName" type="text" value='${user.userInfo.firstName}' readonly>
            <label for="lastName">Last Name: </label>
            <input class="mainDetails" name="lastName" type="text" value='${user.userInfo.lastName}'' readonly>
            <label for="gender">Gender: </label>
            <input class="mainDetails" name="gender" type="text" value='${user.userInfo.gender}' readonly>
            <label for="age">Age: </label>
            <input class="mainDetails" name="age" class="age" type="number" value='${user.userInfo.age}' readonly>
            <div class="btns">
            <button class="editBtn" value=${user.userInfo.id}>${user.edditHobbieBtn.label}</button>
            <button class="saveBtn" value=${user.userInfo.id}>Save</button>
            </div>
          <label for="hobbies">${user.userInfo.hobbies.hobbiesTitle}</label>
              <ul class="hobbiesContainer">${user.userInfo.hobbies.getHobbies(user.userInfo.hobbies.hobbiesItems)} </ul>
              <div class="addContainer">
            <button class="addHobbie" value=${user.userInfo.id}>${user.addHobbieBtnLabel}</button>
            <input class="newHobbie" value="" >
            </div>
            
            <div class="container-pastPhotos imgWrapper">
              <h4>Past Pictures</h4>
              <button class="backArrow" value=${user.userInfo.id}><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve">
                <g><path d="M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225
              		c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
  	          	</button>
           
             <img class="pastPhoto"src='${user.userInfo.pastImages[0]}';
              alt='Past Photo'/>
              
              <button value=${user.userInfo.id} class="nextArrow"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             	 viewBox="0 0 477.175 477.175" style="enable-background:new 0 0 477.175 477.175;" xml:space="preserve">
               <g><path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5
      		       c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z
      		       "/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
               </button>
              </div>
           </div
          </div>
           `;

                usersContainer.appendChild(userContainer);
                document.querySelector("#goBack").addEventListener("click", function () {
                    window.location.href = `../../common/index.html`;
                });
            });

            document.querySelectorAll(".addHobbie").forEach(addBtn => {
                addBtn.addEventListener("click", function () {
                    addHobbie(this);
                });
            });

            document.querySelectorAll(".removeBtn").forEach(element => element.addEventListener("click", function () {
                let parent = element.parentElement.parentElement.parentElement.parentElement;
                removeHobbie(parent.id);
            }));

            document.querySelectorAll(".editBtn").forEach(btn => btn.addEventListener("click", function () {
                editDetails(this.value);

            }));

            document.querySelectorAll(".saveBtn").forEach(btn => btn.addEventListener("click", function () {
                saveDetails(this.value);
            }));


            document.querySelectorAll(".nextArrow").forEach(nextArrow => nextArrow.addEventListener("click", function () {
                renderNextPicture(this.value);
            }));

            document.querySelectorAll(".backArrow").forEach(nextArrow => nextArrow.addEventListener("click", function () {
                renderPreviousPicture(this.value);
            }));

        },


    };

    let newId = 0;
    let counter = 0;

    const changePicture = () => {
        let photoInput = document.querySelector(".photoInput");
        let parent = photoInput.parentElement.parentElement;
        if (photoInput.value !== "") users[parent.id].userInfo.image.src = photoInput.value;
    };

    const saveDetails = (id) => {
        newId = 0;
        let container = document.querySelectorAll(".user-details")[id];
        let inputs = container.querySelectorAll(".mainDetails");
        changePicture();
        inputs.forEach(input => users[id].userInfo[input.name] = input.value);
        updatePage("updated", users[id]);
    };


    const createPhotoInput = (id) => {
        let container = document.querySelectorAll(".user-details")[id];
        let sibling = container.querySelector('[name=age]');
        let imageUrl = document.createElement("input");
        imageUrl.placeholder = "Insert photo url";
        let editImgLabel = document.createElement("label");
        editImgLabel.innerHTML = "Photo Url: ";
        imageUrl.className = "photoInput";
        sibling.parentNode.insertBefore(imageUrl, sibling.nextSibling);
        imageUrl.parentNode.insertBefore(editImgLabel, sibling.nextSibling);
    };

    const editDetails = ((id) => {
        return (id) => {
            if (newId !== id) {
                newId = id;
                createPhotoInput(id);
            }
            let container = document.querySelectorAll(".user-details")[id];
            let inputs = container.querySelectorAll(".mainDetails");
            container.querySelector(".saveBtn").style.visibility = "visible";
            inputs.forEach(input => input.readOnly = false);
        };

    })();


    const addHobbie = (index) => {
        let newHobbie = document.querySelectorAll(".newHobbie");
        newHobbie.forEach((input, i) => {
            if (i === Number(index.value) && input.value !== "") users[index.value].userInfo.hobbies.hobbiesItems.push(input.value);
        });
        updatePage("added", users[index.value]);
    };


    const removeHobbie = (index) => {
        users.forEach((user, i) => {
            if (Number(index) === i) {
                users[i].userInfo.hobbies.hobbiesItems = users[i].userInfo.hobbies.hobbiesItems.filter((hobbie, index) => index !== Number(event.target.value));
            }
        });

        updatePage("removed", users[index]);
    };

    const updatePage = (action, user) => {
        user.userInfo.hobbies.getHobbies(user.userInfo.hobbies.hobbiesItems);
        usersDashboard.displayDetails();

    };


    const renderNextPicture = ((id) => {


        return (id) => {
            if (newId !== id) {
                counter = 0;
                newId = id;
            }
            if (counter >= users[id].userInfo.pastImages.length - 1) {
                return
            }

            if (counter++ < users[id].userInfo.pastImages.length - 1) {
                let pastPhoto = document.querySelectorAll(".pastPhoto")[id];
                pastPhoto.src = " " + users[id].userInfo.pastImages[counter] + " ";
            }

        };
    })();

    const renderPreviousPicture = ((id) => {
        return (id) => {
            if (newId !== id) {
                counter = 0;
                newId = id;
            }
            if (counter <= 0) {
                return
            }

            if (counter-- <= users[id].userInfo.pastImages.length - 1) {
                let pastPhoto = document.querySelectorAll(".pastPhoto")[id];
                pastPhoto.src = " " + users[id].userInfo.pastImages[counter] + " ";
            }
        };
    })();

    usersDashboard.displayDetails();

} catch (e) {
    console.log(e);
}
