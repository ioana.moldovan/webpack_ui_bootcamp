const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const fs = require('fs');
const sourcePath = './src';
const result = {};
const folders = []

function getEntries(source) {
    return fs.readdirSync(source)
        .forEach(
            (file) => {
                console.log("s", file)
                if (file.indexOf('.') === -1) {
                    folders.push(file)
//     getEntries(source + '/' + file)
                    //     folders[file] = file;
                    // } else if(file.indexOf('.js') > -1) {
                    //     result[file.substr(0, file.length-3)] = source + '/' +  file;
                }
            }
        )
}


getEntries(sourcePath);

const getPaths = folders.forEach(folder => {
    return path.resolve(__dirname, 'dist/' + folder + '/')
})
// console.log(result);
console.log("folders", folders);

module.exports = [
    {
        entry: './src/index.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: "index.js"
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.html$/i,
                    loader: 'html-loader'

                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: "./src/index.html"
            })

        ]
    },
    {
        entry: {
            'myprofile_ioana1': './src/ioana/myprofile_ioana1.js',
            'ioana_user_dashboard': './src/ioana/ioana_user_dashboard.js'
        },
        output: {
            path: path.resolve(__dirname, 'dist/ioana/'),
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.html$/i,
                    loader: 'html-loader'
                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                filename: 'ioana_moldovan.html',
                template: "./src/ioana/ioana_moldovan.html",
                chunks: ['myprofile_ioana1']
            }),
            new HtmlWebpackPlugin({
                template: "./src/ioana/ioana_user_dashboard.html",
                filename: 'ioana_user_dashboard.html',
                chunks: ['ioana_user_dashboard']
            })

        ]
    },

    {
        entry: {
            'aboutMV3': './src/mariana/aboutMV3.js',
        },
        output: {
            path: path.resolve(__dirname, 'dist/mariana/'),
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.html$/i,
                    loader: 'html-loader'

                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                filename: 'mariana.html',
                template: "./src/mariana/mariana.html"
            })
            // new CleanWebpackPlugin()

        ]
    },
    {
        entry: {
            'dashboard': './src/mariana/users/dashboard.js'
        },
        output: {
            path: path.resolve(__dirname, 'dist/mariana/users'),
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.html$/i,
                    loader: 'html-loader'

                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                filename: 'dashboard.html',
                template: "./src/mariana/users/dashboard.html"
            })
        ]


    }



    // entry: {...folders},

    // output: {
    //     path: getPaths
    //     },


    // entry: {...result},
    //
    // output: {
    //     chunkFilename: (path) => {
    //         return path.chunk.name === '' ? '[name].js': '[name]/[name].js';
    //     },
    //
    //     // publicPath: ""
    //     // path: path.resolve( __dirname, '../dist'),
    // },

    // plugins: [
    //     new HtmlWebpackPlugin({
    //     template: "./src/index.html"
    //     }),
    //     // new HtmlWebpackPlugin({
    //     //     template: "./src/ioana/ioana_moldovan.html",
    //     //     chunks: ['ioana_profile']
    //     // }),
    //     new CleanWebpackPlugin()
    //
    //
    // ],

    // module: {
    //     rules: [
    //         {
    //             test: /\.js$/,
    //             exclude: /node_modules/,
    //             use: {
    //                 loader: "babel-loader"
    //             }
    //         },
    //         {
    //             test: /\.css$/i,
    //             use: ['style-loader', 'css-loader'],
    //         },
    //         {
    //             test: /\.html$/i,
    //             loader: 'html-loader',
    //
    //         }
    //     ]
    // }
// }
]